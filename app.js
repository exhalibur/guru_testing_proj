
var express = require('express');
var bodyParser = require('body-parser');

const { sql } = require('./db_connector.js') //Модуль SQL запросов к БД
var Validator = require('jsonschema').Validator; //Валидатор данных
const fs = require('fs') // Файловая система для декодинга base64
const jsonParser = express.json(); //Парсер данных body

var app = express();
var v = new Validator();
app.listen(3000);
var useExecute = true;

app.use(function (req, res, next) {
    // Устанавливаем CORS Policy
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true, parameterLimit: 50000 }));

app.get('/card', jsonParser, function (req, res) {
    // API для получения информации по id товара
    // req - запрос
    // req - ответ
    var tovarIdSchema = {
        // Валидатор тела запроса
        "id": "/tovarIdSchema",
        "type": "object",
        "properties": {
          "id": {"type": "number"},         
        },
        "required": ["id"]
      };
    req_body = (v.validate(req.body, tovarIdSchema));
    if ((req_body.errors).length != 0){
        res.json({
            success: false,
            error: req_body.errors
        })
        return;
    }

    // Логика программы
    sql("SELECT * FROM tovar WHERE id = ?", useExecute, [req.body.id]).then(tovarInfo =>{
        if (tovarInfo.length == 0){
            res.json({
                success: true,
                error: '',
                result: []
            })
            return;
        }
        var result = {
            id: tovarInfo[0].id,
            title: tovarInfo[0].title,
            description: tovarInfo[0].description,
            images: [],
        };
        sql("SELECT url from images WHERE tovar_id = ?", useExecute,
            [req.body.id]).then(imageInfo =>{
                result.images = imageInfo;
                return result;

            }).then(result =>{
                res.json({
                    success: true,
                    error: '',
                    result: result
                })
                return;
            });   
        
    });
});
app.post('/card', jsonParser, function(req, res){
    // API для добавления товара
    // req - запрос
    // req - ответ
    var imagesSchema = {
        // Валидатор массива картинок
        "id": "/SimpleImage",
        "type": "array",
        "items" : {
            "type": "object",
            "properties":{
                "base64StringFile": {"type": "string"},
                "fileName": {"type": "string"},
                "fileType": {"type": "string"}
            }
        }
      };
      
      var tovarSchema = {
        // Валидатор Товара
        "id": "/SimpleTovar",
        "type": "object",
        "properties": {
          "tovar_name": {"type": "string"},
          "tovar_desc": {"type": "string"},
          "images": {"$ref": "/SimpleImage"},
          
        },
        "required": ["tovar_name","tovar_desc","images"]
      };
    
    v.addSchema(imagesSchema, '/SimpleImage');
    req_body = (v.validate(req.body, tovarSchema));

    if ((req_body.errors).length != 0){
        res.json({
            success: false,
            error: req_body.errors
        })
        return;
    }
    //Логика добавления в БД карточки
    let params = [req.body.tovar_name, req.body.tovar_desc]
        
    sql('INSERT INTO tovar(title, description) VALUES(?, ?)', !useExecute,  params)
    .then(tovarId =>{
        let string_values = '';
        for (let i = 0; i < (req.body.images).length; i++){
            let base64String = 'data:'+(req.body.images)[i].fileType+';base64,'+(req.body.images)[i].base64StringFile; 
            let base64Image = base64String.split(';base64,').pop();
            let photoPath = 'images/'+(req.body.images)[i].fileName
            fs.writeFile(photoPath, base64Image, {encoding: 'base64'});

            string_values += 'VALUES("'+tovarId+'","'+photoPath+'","'+(req.body.images)[i].fileName+'", "'+(req.body.images)[i].fileType+'"),'
        }
        let str = "INSERT INTO images(tovar_id, url, img_name, img_type) "+string_values.substr(0, string_values.length-1)
        sql(str,!useExecute)
    })
    res.json({
        success: true,
        error: ''
    })
    return;
})