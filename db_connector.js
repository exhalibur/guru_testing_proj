const mysql = require('mysql2/promise');
async function sql(queryes, useExecute, params=[]){
    var result = null;
    const connection = await mysql.createConnection({
        host: "localhost",
        user: "my_user",
        database: "product",
        password: "pass123"
      });
      connection.connect(function(err){

        if (err) {
          return console.error("Ошибка: " + err.message);
        }
        else{
          console.log("Подключение к серверу MySQL успешно установлено");
        }
     });
    
    if(useExecute){
        await connection.execute(queryes, params)
        .then(data =>{
            result = data[0];
            }
        )
    }
    else{
        await connection.query(queryes, params)
        .then(data=>{
            result = data[0].insertId;
        })
    }
    
    
    await connection.end(function(err) {
      // закрытие подключения
      if (err) {
        return console.log("Ошибка: " + err.message);
      }     
    });
    return result;
}
module.exports = { sql }