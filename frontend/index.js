function getFiles(files) {
    let temp_arr = [];
    for (let i = 0; i < files.length; i++) {
        temp_arr.push(getFile(files[i]))
    }
    return Promise.all(temp_arr)
}
function getFile(file) {
    let reader = new FileReader();
    return new Promise((resolve, reject) => {
        reader.onerror = () => { reader.abort(); reject(new Error("Error parsing file")); }
        reader.onload = function () {
            let bytes = Array.from(new Uint8Array(this.result));
            let base64StringFile = btoa(bytes.map((item) => String.fromCharCode(item)).join(""));
            resolve({
                base64StringFile: base64StringFile,
                fileName: file.name,
                fileType: file.type
            });
        }
        reader.readAsArrayBuffer(file);
    });
}


$(document).ready(function () {
    
    $('.post_product').click(function () {
        var inputs = $('input[type=file]')[0].files;
        getFiles(inputs).then(data => {
            fetch('http://127.0.0.1:3000/card', {
                method: "POST",
                headers: {
                    "content-type": "application/json",
                },
                body: JSON.stringify({
                    tovar_name: ($('input#title').val()).toString(),
                    tovar_desc: ($('textarea#description').val()).toString(),
                    images: data,
                })
            }).then(response =>{
                return response.json();
            }).then(data =>{
                if(data.success){
                    alert('Продукт успешно добавлен')
                }
                else{
                    console.log(data.error)
                }
            })
        })
    })
})